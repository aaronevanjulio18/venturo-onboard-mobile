import 'package:get/get.dart';
import 'package:onboard_venturo/config/routes/app_routes.dart';
import 'package:onboard_venturo/modules/features/cart/view/ui/cart_view.dart';
import 'package:onboard_venturo/modules/features/cart/view/ui/choose_voucher_view.dart';
import 'package:onboard_venturo/modules/features/cart/view/ui/detail_voucher_view.dart';
import 'package:onboard_venturo/modules/features/dashboard/controllers/dashboard_binding.dart';
import 'package:onboard_venturo/modules/features/dashboard/view/dashboard_view.dart';
import 'package:onboard_venturo/modules/features/login/controllers/login_binding.dart';
import 'package:onboard_venturo/modules/features/login/view/ui/login_view.dart';
import 'package:onboard_venturo/modules/features/menu/controllers/detail_menu_binding.dart';
import 'package:onboard_venturo/modules/features/menu/view/ui/detail_menu_view.dart';
import 'package:onboard_venturo/modules/features/order/controllers/detail_order_binding.dart';
import 'package:onboard_venturo/modules/features/order/view/ui/detail_order_view.dart';
import 'package:onboard_venturo/modules/features/promo/controllers/detail_promo_binding.dart';
import 'package:onboard_venturo/modules/features/promo/view/ui/detail_promo_view.dart';
import '../../modules/features/splash/controllers/splash_binding.dart';
import '../../modules/features/splash/view/splash_view.dart';

class AppPages {
  /// memastikan bahwa kelas ini tidak bisa diinstansiasi
  AppPages._();

  static List<GetPage> pages() {
    return [
      /// Authentication
      GetPage(
        name: AppRoutes.splashView,
        page: () => const SplashView(),
        binding: SplashBinding(),
      ),
      GetPage(
        name: AppRoutes.loginView, 
        page: () => LoginView(), 
        binding: LoginBinding(),
        /// add transition
        transition: Transition.fadeIn,
        transitionDuration: const Duration(milliseconds: 1700),
      ),
      GetPage(
        name: AppRoutes.dashboardView, 
        page: () => const DashboardView(), 
        binding: DashboardBinding(),
      ),
      /// Menu dan Promo
      GetPage(
        name: AppRoutes.detailPromoView,
        page: () => const DetailPromoView(),
        binding: DetailPromoBinding(),
      ),
      GetPage(
        name: AppRoutes.detailMenuView,
        page: () => const DetailMenuView(),
        binding: DetailMenuBinding(),
      ),

      /// Cart
      GetPage(
        name: AppRoutes.cartView,
        page: () => const CartView(),
      ),
      GetPage(
        name: AppRoutes.chooseVoucherView,
        page: () => const ChooseVoucherView(),
      ),
      GetPage(
        name: AppRoutes.detailVoucherView,
        page: () => const DetailVoucherView(),
      ),

      /// Order
      GetPage(
        name: AppRoutes.detailOrderView,
        page: () => const DetailOrderView(),
        binding: DetailOrderBinding(),
      ),

      /// Review
      // GetPage(
      //   name: AppRoutes.reviewView,
      //   page: () => ReviewView(),
      //   binding: ReviewBinding(),
      // ),
      // GetPage(
      //   name: AppRoutes.addReviewView,
      //   page: () => AddReviewView(),
      //   binding: AddReviewBinding(),
      // ),
      // GetPage(
      //   name: AppRoutes.replyReviewView,
      //   page: () => ReplyReviewView(),
      //   binding: ReplyReviewBinding(),
      // ),
    ];
  }
}
