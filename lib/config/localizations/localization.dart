import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:onboard_venturo/config/localizations/langs/en_us.dart';
import 'package:onboard_venturo/config/localizations/langs/id_id.dart';
import 'package:onboard_venturo/constant/cores/asset_const.dart';

class Localization extends Translations {
  /// setting locale default
  static const locale = Locale('id', 'ID');

  /// add fallback locale
  static const fallbackLocale = Locale('en', 'US');
  
  /// List support language
  static const langs = [
    'English',
    'Indonesia',
  ];

  /// list asset path langs
  static const flags = [
    AssetConst.flagEN,
    AssetConst.flagID,
  ];

  /// list locale
  static const locales = [
    Locale('en', 'US'),
    Locale('id', 'ID'),
  ];

  /// mapping locale
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': translations_en_US,
        'id_ID': translations_id_ID,
      };

  /// change locale
  static void changeLocale(String lang) {
    final locale = getLocaleFromLanguage(lang);
    Get.updateLocale(locale);
  }

  /// find lang and return
  static Locale getLocaleFromLanguage(String lang) {
    for (int i = 0; i < langs.length; i++) {
      if (lang == langs[i]) return locales[i];
    }

    return currentLocale;
  }

  /// return curre nt locale
  static Locale get currentLocale {
    return Get.locale ?? fallbackLocale;
  }

  /// return current lang
  static String get currentLanguage {
    return langs.elementAt(locales.indexOf(currentLocale));
  }
}
