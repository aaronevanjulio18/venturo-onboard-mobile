import 'package:get/get.dart';
import 'package:onboard_venturo/modules/global_controllers/global_controllers.dart';

class GlobalBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(GlobalController(), permanent: true);
  }
}
