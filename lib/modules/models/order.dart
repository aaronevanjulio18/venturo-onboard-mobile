// ignore_for_file: non_constant_identifier_names

import 'package:equatable/equatable.dart';
import 'package:onboard_venturo/modules/models/detail_order.dart';

class Order extends Equatable {
  final int idOrder;
  final String noStruk;
  final String nama;
  final int idVoucher;
  final String namaVoucher;
  final int diskon;
  final int potongan;
  final int totalBayar;
  final DateTime tanggal;
  final int status;
  final List<DetailOrder> menu;

  const Order({
    required this.idOrder,
    required this.noStruk,
    required this.nama,
    required this.idVoucher,
    required this.namaVoucher,
    required this.diskon,
    required this.potongan,
    required this.totalBayar,
    required this.tanggal,
    required this.status,
    required this.menu,
  });

  /// Mendapatkan total harga tanpa potongan
  int get total => totalBayar + potongan;

  /// From json
  factory Order.fromJson(Map<String, dynamic> json) {
    return Order(
      idOrder: json['id_order'] as int,
      noStruk: json['no_struk'] as String,
      nama: (json['nama'] ?? '') as String,
      idVoucher: (json['id_voucher'] ?? 0) as int,
      namaVoucher: (json['nama_voucher'] ?? '') as String,
      diskon: (json['diskon'] ?? 0) as int,
      potongan: (json['potongan'] ?? 0) as int,
      totalBayar: json['total_bayar'] as int,
      tanggal: DateTime.parse(json['tanggal'] as String),
      status: json['status'] as int,
      menu: (json['menu'] as List)
          .map<DetailOrder>((e) => DetailOrder.fromJson(e))
          .toList(),
    );
  }

  @override
  List<Object?> get props => [idOrder];
}

class ListOrderRes {
  final int statusCode;
  final List<Order>? data;

  const ListOrderRes({
    required this.statusCode,
    this.data,
  });

  /// From json
  factory ListOrderRes.fromJson(Map<String, dynamic> json) {
    return ListOrderRes(
      statusCode: json['status_code'] as int,
      data: json['status_code'] == 200
          ? json['data'].map<Order>((e) => Order.fromJson(e)).toList()
          : null,
    );
  }
}

class OrderRes {
  final int statusCode;
  final Order? data;

  const OrderRes({
    required this.statusCode,
    this.data,
  });

  /// From json
  factory OrderRes.fromJson(Map<String, dynamic> json) {
    if (json['status_code'] == 200) {
      json['data']['order']['menu'] = json['data']['detail'];

      return OrderRes(
        statusCode: json['status_code'] as int,
        data: Order.fromJson(json['data']['order']),
      );
    } else {
      return OrderRes(statusCode: json['status_code'] as int);
    }
  }
}
