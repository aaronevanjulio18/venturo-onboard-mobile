import 'package:equatable/equatable.dart';
import 'package:onboard_venturo/utils/extension/date_extension.dart';

class User extends Equatable {
  final int idUser;
  final String email;
  final String nama;
  final DateTime? tglLahir;
  final String? alamat;
  final String? telepon;
  final String? ktp;
  final String pin;
  final String? foto;

  const User({
    required this.idUser,
    required this.email,
    required this.nama,
    required this.tglLahir,
    required this.alamat,
    required this.telepon,
    required this.ktp,
    required this.pin,
    required this.foto,
  });

  /// From Json
  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      idUser: json['id_user'] as int,
      email: json['email'] as String,
      nama: json['nama'] as String,
      tglLahir: json['tgl_lahir'] != null
          ? DateTime.parse(json['tgl_lahir'] as String)
          : null,
      alamat: json['alamat'] as String?,
      telepon: json['telepon'] as String?,
      ktp: json['ktp'] as String?,
      pin: json['pin'] as String,
      foto: json['foto'] as String?,
    );
  }

  /// To Map
  Map<String, dynamic> toMap() {
    return {
      'id_user': idUser,
      'email': email,
      'nama': nama,
      'tgl_lahir': tglLahir == null ? null : tglLahir!.toDateString(),
      'alamat': alamat,
      'tele pon': telepon,
      'ktp': ktp,
      'pin': pin,
      'foto': foto,
    };
  }

  static User dummy = const User(
    idUser: 0,
    email: '',
    nama: '',
    tglLahir: null,
    alamat: '',
    telepon: '',
    ktp: '',
    pin: '',
    foto: '',
  );

  @override
  List<Object?> get props => [idUser];
}

class UserRes {
  final int statusCode;
  final String? message;
  final User? user;
  final String? token;

  const UserRes({
    required this.statusCode,
    this.message,
    this.user,
    this.token,
  });

  /// From Login Json
  factory UserRes.fromLoginJson(Map<String, dynamic> json) {
    return UserRes(
      statusCode: json['status_code'],
      message: json['message'],
      user: json['status_code'] == 200
          ? User.fromJson(json['data']['user'])
          : null,
      token: json['status_code'] == 200 ? json['data']['token'] : null,
    );
  }

  /// From Login Json
  factory UserRes.fromJson(Map<String, dynamic> json) {
    return UserRes(
      statusCode: json['status_code'],
      message: json['message'],
      user: json['status_code'] == 200 ? User.fromJson(json['data']) : null,
    );
  }
}
