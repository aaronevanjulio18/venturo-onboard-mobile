// ignore_for_file: non_constant_identifier_names

import 'package:equatable/equatable.dart';

class Voucher extends Equatable {
  final int idVoucher;
  final int idUser;
  final String nama;
  final String namaUser;
  final int nominal;
  final String infoVoucher;
  final DateTime periodeMulai;
  final DateTime periodeSelesai;
  final int type;
  final int status;
  final String? catatan;

  const Voucher({
    required this.idVoucher,
    required this.idUser,
    required this.nama,
    required this.namaUser,
    required this.nominal,
    required this.infoVoucher,
    required this.periodeMulai,
    required this.periodeSelesai,
    required this.type,
    required this.status,
    required this.catatan,
  });

  /// From json
  factory Voucher.fromJson(Map<String, dynamic> json) {
    return Voucher(
      idVoucher: json['id_voucher'] as int,
      idUser: json['id_user'] as int,
      nama: json['nama'] as String,
      namaUser: json['nama_user'] as String,
      nominal: json['nominal'] as int,
      infoVoucher: json['info_voucher'] as String,
      periodeMulai: DateTime.parse(json['periode_mulai'] as String),
      periodeSelesai: DateTime.parse(json['periode_selesai'] as String),
      type: json['type'] as int,
      status: json['status'] as int,
      catatan: json['catatan'] as String,
    );
  }

  @override
  List<Object?> get props => [idVoucher];
}

class ListVoucherRes {
  final int statusCode;
  final String? message;
  final List<Voucher>? data;

  ListVoucherRes({
    required this.statusCode,
    this.message,
    this.data,
  });

  /// From json
  factory ListVoucherRes.fromJson(Map<String, dynamic> json) {
    return ListVoucherRes(
      statusCode: json['status_code'] as int,
      message: json['message'] as String?,
      data: json['status_code'] == 200
          ? json['data'].map<Voucher>((e) => Voucher.fromJson(e)).toList()
          : null,
    );
  }
}
