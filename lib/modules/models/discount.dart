// ignore_for_file: non_constant_identifier_names

import 'package:equatable/equatable.dart';

class Discount extends Equatable {
  final int idDiskon;
  final int idUser;
  final String namaUser;
  final String nama;
  final int nominal;

  const Discount({
    required this.idDiskon,
    required this.idUser,
    required this.namaUser,
    required this.nama,
    required this.nominal,
  });

  /// From json
  factory Discount.fromJson(Map<String, dynamic> json) {
    return Discount(
      idDiskon: json['id_diskon'] as int,
      idUser: json['id_user'] as int,
      namaUser: json['nama_user'] as String,
      nama: json['nama'] as String,
      nominal: json['nominal'] as int,
    );
  }

  /// To map
  Map<String, dynamic> toMap() {
    return {
      'id_diskon': idDiskon,
      'id_user': idUser,
      'nama_user': namaUser,
      'nama': nama,
      'nominal': nominal,
    };
  }

  @override
  List<Object?> get props => [idDiskon];
}

class ListDiscountRes {
  final int statusCode;
  final String? message;
  final List<Discount>? data;

  const ListDiscountRes({
    required this.statusCode,
    this.message,
    this.data,
  });

  /// From json
  factory ListDiscountRes.fromJson(Map<String, dynamic> json) {
    return ListDiscountRes(
      statusCode: json['status_code'] as int,
      message: json['message'] as String?,
      data: json['status_code'] == 200
          ? json['data'].map<Discount>((e) => Discount.fromJson(e)).toList()
          : null,
    );
  }
}
