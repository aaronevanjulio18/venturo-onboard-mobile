import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:onboard_venturo/constant/cores/api_const.dart';
import 'package:onboard_venturo/modules/models/discount.dart';
import 'package:onboard_venturo/modules/models/user.dart';
import 'package:onboard_venturo/utils/services/api_services.dart';
import 'package:onboard_venturo/utils/services/local_db_services.dart';

class DiscountRepository {
  DiscountRepository._();

  /// Memanggil API untuk mendapatkan semua menu
  static Future<ListDiscountRes> getAll() async {
    try {
      var dio = ApiServices.dioCall(token: await LocalDBServices.getToken());
      var user = await LocalDBServices.getUser() as User;
      var response = await dio.get(
        '${ApiConst.allDiscountPerUser}/${user.idUser}',
      );

      return ListDiscountRes.fromJson(response.data);
    } on DioError {
      return ListDiscountRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
