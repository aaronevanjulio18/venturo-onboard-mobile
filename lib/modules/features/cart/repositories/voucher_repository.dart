import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:onboard_venturo/constant/cores/api_const.dart';
import 'package:onboard_venturo/modules/models/user.dart';
import 'package:onboard_venturo/modules/models/voucher.dart';
import 'package:onboard_venturo/utils/services/api_services.dart';
import 'package:onboard_venturo/utils/services/local_db_services.dart';

class VoucherRepository {
  VoucherRepository._();

  /// Memanggil API untuk mendapatkan semua menu
  static Future<ListVoucherRes> getAll() async {
    try {
      var dio = ApiServices.dioCall(token: await LocalDBServices.getToken());
      var user = await LocalDBServices.getUser() as User;
      var response = await dio.get(
        '${ApiConst.allVoucherPerUser}/${user.idUser}',
      );

      return ListVoucherRes.fromJson(response.data);
    } on DioError {
      return ListVoucherRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
