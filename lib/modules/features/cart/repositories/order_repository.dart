import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:onboard_venturo/constant/cores/api_const.dart';
import 'package:onboard_venturo/modules/models/cart_item.dart';
import 'package:onboard_venturo/utils/services/api_services.dart';
import 'package:onboard_venturo/utils/services/local_db_services.dart';

class OrderRepository {
  OrderRepository._();

  /// Memanggil API untuk mendapatkan semua menu
  static Future<Response?> add(CartReq cartReq) async {
    try {
      var dio = ApiServices.dioCall(token: await LocalDBServices.getToken());
      var response = await dio.post(
        ApiConst.addOrder,
        data: json.encode(cartReq.toMap()),
      );

      return response;
    } on DioError {
      return null;
    }
  }
}
