import 'package:get/get.dart';
import 'package:onboard_venturo/config/localizations/localization.dart';
import 'package:onboard_venturo/config/routes/app_routes.dart';
import 'package:onboard_venturo/modules/global_controllers/global_controllers.dart';
import 'package:onboard_venturo/utils/services/local_db_services.dart';

class SplashController extends GetxController {
  static SplashController get to => Get.find<SplashController>();

  @override
  Future<void> onInit() async {
    super.onInit();

    //duration splash screen
    await Future.delayed(const Duration(milliseconds: 1000));

    //get lang
    var language = await LocalDBServices.getLanguage();
    if (language != null) {
      Localization.changeLocale(language);
    }

    //cek koneksi internet
    if (!GlobalController.to.internetStatus.value) {
      await GlobalController.to.showAlert();
    }

    // mendapatkan user dan token dari local DB service
    var user = await LocalDBServices.getUser();
    var token = await LocalDBServices.getToken();
    // jika ada sesi login
    if (user != null && token != null) {
      await Get.offAllNamed(AppRoutes.dashboardView);
    } else {
      Get.offAllNamed(AppRoutes.loginView);
    }
  }
}
