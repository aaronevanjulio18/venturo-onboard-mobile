import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:onboard_venturo/constant/cores/api_const.dart';
import 'package:onboard_venturo/modules/models/promo.dart';
import 'package:onboard_venturo/utils/services/api_services.dart';
import 'package:onboard_venturo/utils/services/local_db_services.dart';

class PromoRepository {
  PromoRepository._();

  /// Memanggil API untuk mendapatkan promo berdasarkan id
  static Future<PromoRes> getFromId(int idPromo) async {
    try {
      var dio = ApiServices.dioCall(token: await LocalDBServices.getToken());
      var response = await dio.get('${ApiConst.detailPromo}/$idPromo');

      return PromoRes.fromJson(response.data);
    } on DioError {
      return PromoRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
