import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:onboard_venturo/constant/cores/api_const.dart';
import 'package:onboard_venturo/modules/models/menu.dart';
import 'package:onboard_venturo/utils/services/api_services.dart';
import 'package:onboard_venturo/utils/services/local_db_services.dart';

class MenuRepository {
  MenuRepository._();

  /// Memanggil API untuk mendapatkan menu berdasarkan id
  static Future<MenuRes> getFromId(int idMenu) async {
    try {
      var dio = ApiServices.dioCall(token: await LocalDBServices.getToken());
      var response = await dio.get('${ApiConst.detailMenu}/$idMenu');

      return MenuRes.fromJson(response.data);
    } on DioError {
      return MenuRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
