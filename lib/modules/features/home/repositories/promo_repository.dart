import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:onboard_venturo/constant/cores/api_const.dart';
import 'package:onboard_venturo/modules/models/promo.dart';
import 'package:onboard_venturo/utils/services/api_services.dart';
import 'package:onboard_venturo/utils/services/local_db_services.dart';

class PromoRepository {
  PromoRepository._();

  /// Memanggil API untuk mendapatkan semua promo
  static Future<ListPromoRes> getAllByUser() async {
    try {
      var dio = ApiServices.dioCall(token: await LocalDBServices.getToken());
      var response = await dio.get(ApiConst.allPromo);

      return ListPromoRes.fromJson(response.data);
    } on DioError {
      return ListPromoRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}