import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:onboard_venturo/constant/cores/api_const.dart';
import 'package:onboard_venturo/modules/models/menu.dart';
import 'package:onboard_venturo/utils/services/api_services.dart';
import 'package:onboard_venturo/utils/services/local_db_services.dart';

class MenuRepository {
  MenuRepository._();

  /// Memanggil API untuk mendapatkan semua menu
  static Future<ListMenuRes> getAll() async {
    try {
      var dio = ApiServices.dioCall(token: await LocalDBServices.getToken());
      var response = await dio.get(ApiConst.allMenu);

      return ListMenuRes.fromJson(response.data);
    } on DioError {
      return ListMenuRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
