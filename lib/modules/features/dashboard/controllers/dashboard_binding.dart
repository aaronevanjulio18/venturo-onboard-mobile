import 'package:get/instance_manager.dart';
import 'package:onboard_venturo/modules/features/cart/controllers/cart_controller.dart';
import 'package:onboard_venturo/modules/features/dashboard/controllers/dashboard_controllers.dart';
import 'package:onboard_venturo/modules/features/home/controllers/home_controllers.dart';
import 'package:onboard_venturo/modules/features/order/controllers/order_controller.dart';
import 'package:onboard_venturo/modules/features/profile/controllers/profile_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    /// Inisialisasi dashboard controller
    Get.put(DashboardController());

    /// Inisialisasi home, order, profile, cart
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => OrderController());
    Get.lazyPut(() => ProfileController());
    Get.lazyPut(() => CartController());
  }
}
