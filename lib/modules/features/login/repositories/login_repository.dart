import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:onboard_venturo/constant/cores/api_const.dart';
import 'package:onboard_venturo/modules/models/user.dart';
import 'package:onboard_venturo/utils/services/api_services.dart';

class LoginRepository {
  LoginRepository._();

  /// instance dio dengan apiservices
  static final Dio _dio = ApiServices.dioCall();

  /// method login
  static Future<UserRes> getUser(String email, String password) async {
    try {
      /// login api with post email and password
      var response = await _dio.post(ApiConst.login, data: {
        'email': email,
        'password': password,
      });

      return UserRes.fromLoginJson(response.data);
    } on DioError {
      return UserRes(statusCode: 500, message: 'Server error'.tr);
    }
  }

  /// method login via google
  static Future<UserRes> getUserFromGoogle(String nama, String email) async {
    try {
      var response = await _dio.post(ApiConst.login, data: {
        'nama': nama,
        'email': email,
        'is_google': 'is_google',
      });

      return UserRes.fromLoginJson(response.data);
    } on DioError {
      return UserRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
