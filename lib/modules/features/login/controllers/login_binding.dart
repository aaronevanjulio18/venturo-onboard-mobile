import 'package:get/instance_manager.dart';
import 'package:onboard_venturo/modules/features/login/controllers/login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(LoginController());
  }
}
