import 'package:get/get.dart';
import 'package:onboard_venturo/config/routes/app_routes.dart';
import 'package:onboard_venturo/modules/features/login/repositories/login_repository.dart';
import 'package:onboard_venturo/modules/models/user.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:onboard_venturo/shared/custom/error_snackbar.dart';
import 'package:onboard_venturo/utils/services/local_db_services.dart';

class LoginController extends GetxController {
  static LoginController get to => Get.find<LoginController>();

  //method login with email and password
  Future<void> loginWithEmailAndPassword(String email, String password) async {
    UserRes userRes = await LoginRepository.getUser(email, password);
    
    if (userRes.statusCode == 200) {
      //set token and user
      await LocalDBServices.setUser(userRes.user!);
      await LocalDBServices.setToken(userRes.token!);
      
      //redirect dashboard page
      Get.offAllNamed(AppRoutes.dashboardView);
    } else if (userRes.statusCode == 422 || userRes.statusCode == 204) {
      //snakcbar wrong email or password
      Get.showSnackbar(
        ErrorSnackBar(
          title: 'Something went wrong'.tr,
          message: userRes.message ?? 'Unknown error'.tr,
        ),
      );
    }
  }

  //login with google account
  Future<void> loginWithGoogle() async {
    //variable google sign in
    final GoogleSignIn googleSignIn = GoogleSignIn();
    await googleSignIn.signOut();

    GoogleSignInAccount? account = await googleSignIn.signIn();
    if (account == null) return;
    //call api repository
    
    UserRes userRes = await LoginRepository.getUserFromGoogle(
        account.displayName ?? '-', account.email);
    
    if (userRes.statusCode == 200) {
      await LocalDBServices.setUser(userRes.user!);
      await LocalDBServices.setToken(userRes.token!);
    
      //redirect dashboard page
      Get.offAllNamed(AppRoutes.dashboardView);
    } else {
      Get.showSnackbar(
        ErrorSnackBar(
          title: 'Something went wrong'.tr, 
          message: 'Unknown error'.tr,
        ),
      );
    }
  }
}
