import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:onboard_venturo/constant/cores/asset_const.dart';

import '../../../../../config/themes/colors.dart';

class LoginButtonGoogle extends StatelessWidget {
  final void Function()? onPressed;

  const LoginButtonGoogle({super.key, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        primary: Colors.white,
        elevation: 4,
        padding: EdgeInsets.symmetric(horizontal: 36.r, vertical: 14.r),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24.r),
        ),
      ),
      child: Row(
        children: [
          SvgPicture.asset(AssetConst.iconGoogle, width: 24.r, height: 24.r),
          const Spacer(),
          Text(
            'Login with '.tr,
            style: GoogleFonts.montserrat(
              fontSize: 14.sp,
              color: AppColor.darkColor,
              fontWeight: FontWeight.w400,
              height: 1.219
            ),
          ),
          Text(
            'Google',
            style: GoogleFonts.montserrat(
              fontSize: 14.sp,
              color: AppColor.darkColor,
              fontWeight: FontWeight.w600,
              height: 1.219
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
