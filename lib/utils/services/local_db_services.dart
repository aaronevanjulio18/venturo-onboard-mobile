import 'dart:convert';

import 'package:onboard_venturo/modules/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalDBServices {
  /// memastikan class tidak bisa diinstansi
  LocalDBServices._();
  
  /// save data user
  static Future<void> setUser(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString('user', json.encode(user.toMap()));
  }

  /// get user data
  static Future<User?> getUser() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? user = sharedPreferences.getString('user');
    
    if (user == null) {
      return null;
    } else {
      return User.fromJson(json.decode(user));
    }
  }

  /// delete user data
  static Future<void> clearUser() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.remove('user');
  }

  /// save token data
  static Future<void> setToken(String token) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString('token', token);
  }

  /// get token data
  static Future<String?> getToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString('token');
  }

  /// delete token data
  static Future<void> clearToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.remove('token');
  }

  /// set lang
  static Future<void> setLanguage(String language) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString('language', language);
  }

  /// get lang
  static Future<String?> getLanguage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString('language');
  }

  /// remove lang
  static Future<void> clearLanguage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.remove('language');
  }
}
