import 'package:intl/intl.dart';

extension DateExtension on DateTime {
  /// date format object
  static final DateFormat _dateTimeFormat = DateFormat('yyyy-MM-dd HH:mm:ss');
  static final DateFormat _dateFormat = DateFormat('yyyy-MM-dd');

  /// date format
  String toDateString() => _dateFormat.format(this);
  
  /// date time format
  String toDateTimeString() => _dateTimeFormat.format(this);
}
