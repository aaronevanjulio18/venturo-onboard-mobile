import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:onboard_venturo/config/themes/colors.dart';
import 'package:onboard_venturo/constant/cores/asset_const.dart';


class NetworkErrorView extends StatelessWidget {
  const NetworkErrorView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              /// judul error
              Text('Error'.tr, style: Get.textTheme.headlineLarge),
              /// icon
              SvgPicture.asset(AssetConst.iconNoInternet, width: 0.6.sw),
              /// pesan error
              Text(
                'Not Connected to the internet'.tr,
                style: Get.textTheme.titleMedium,
                textAlign: TextAlign.center,
              ),
              16.verticalSpacingRadius,
              ///  button konfirmasi
              ElevatedButton(
                onPressed: () => AppSettings.openDeviceSettings(), 
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  elevation: 2,
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(24.r),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(Icons.settings, color: AppColor.darkColor),
                    10.horizontalSpaceRadius,
                    Text(
                      'Open settings'.tr,
                      style: Get.textTheme.bodyMedium,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}